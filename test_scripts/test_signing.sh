#!/bin/bash
#
#Author: Binesh Andrews <bineshandrews@gmail.com>
#
# Description: Provides a systematic test script to prove the signing/ sign-
# verification capabilities of the RSA engine and also test its inter-operability
# with OPENSSL.
#
# Last updated on: 9-OCT-2012
#

# Verifies signing and sign verification, certificate self-signing test, checks
# whether Openssl can parse our output certificate, and does the certifictate
# verification by openssl. 

PROG=../bin/out/rsaeng.out
KEYSIZE=1024

mkdir -p ../testdata

#Generate our keys
openssl genrsa -out ../testdata/private3.pem $KEYSIZE  > /dev/null 2>&1
openssl rsa -in ../testdata/private3.pem -pubout -out ../testdata/public3.pem > /dev/null 2>&1

# Create a dat file
dd if=/dev/urandom of=../testdata/data.dat bs=4096 count=1024


openssl dgst -sha1 -sign ../testdata/private3.pem -out ../testdata/data.sign1 ../testdata/data.dat
$PROG signfile -in ../testdata/data.dat -keyfile ../testdata/private3.pem -signature ../testdata/data.sign2 -out ../testdata/data.cert2

#View and verify certificate
openssl x509 -in ../testdata/data.cert2 -text -noout
$PROG verifysign -in ../testdata/data.cert2
openssl verify ../testdata/data.cert2

#Verify sign

openssl dgst -sha1 -verify ../testdata/public3.pem -signature ../testdata/data.sign1 ../testdata/data.dat
openssl dgst -sha1 -verify ../testdata/public3.pem -signature ../testdata/data.sign2 ../testdata/data.dat

# Should be able to verify sign given either public key or a X509 certificate containing the public key.
$PROG verifyfile -in ../testdata/data.dat -keyfile ../testdata/public3.pem -signature ../testdata/data.sign1 
$PROG verifyfile -in ../testdata/data.dat -certfile ../testdata/data.cert2 -signature ../testdata/data.sign1 
$PROG verifyfile -in ../testdata/data.dat -keyfile ../testdata/public3.pem -signature ../testdata/data.sign2 
$PROG verifyfile -in ../testdata/data.dat -certfile ../testdata/data.cert2 -signature ../testdata/data.sign2 

