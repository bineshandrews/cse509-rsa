/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implements the miller rabin test for primality and then uses it
to generate prime numbers of a given size. The entire functionality is attached
to an iterator to provide a cleaner interface for prime number generation. 

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>

#include "mr_prime_gen.h"

MillerRabinPrimeGenerator :: PrimeGenIterator :: PrimeGenIterator(uint32_t bitsize) : rIter(bitsize) 
{
	prime = 0;
}

MillerRabinPrimeGenerator :: PrimeGenIterator :: PrimeGenIterator(const MillerRabinPrimeGenerator :: PrimeGenIterator &pgtor) : rIter(pgtor.rIter) 
{
	prime = pgtor.prime;
}


bool MillerRabinPrimeGenerator :: PrimeGenIterator :: IsPrime(BIGINTEGER candidate)
{
	uint32_t basic_nums[] = { 2, 3, 5, 7, 11, 13, 17 };
	uint32_t i;

	if (candidate < 2)
		return false;

	for (i = 0; i < sizeof(basic_nums) / sizeof(basic_nums[0]); i++)
	{
		if(!mpz_fdiv_ui(candidate.get_mpz_t(), basic_nums[i]))
			return false;
	}

#ifdef USE_GMP_PRIMES
	return mpz_probab_prime_p(candidate.get_mpz_t(), NUM_WITNESS) != 0;
#else
	//TODO: cleanup all stack variables before returning
	BIGINTEGER witness, temp, pminus1, witpowd;
	uint32_t exp2 = 0, j;
	for (i =0; i < NUM_WITNESS; i++)
	{
		//Select a random number(witness) less than [2, p-2]; 0 is not acceptable (we need to divide!!)
		pminus1 = candidate - 1;
		witness = 2 + rIter.inRange(candidate - 3);

		// Find the exponent of 2 in the factorization. p-1 = (2^s)*d
		for (temp = pminus1; mpz_even_p(temp.get_mpz_t()); temp=temp/2, exp2++);
	
		mpz_powm(witpowd.get_mpz_t(), witness.get_mpz_t(), temp.get_mpz_t(), candidate.get_mpz_t());
		if (witpowd == 1 || witpowd == pminus1)
			continue;

		for (j = exp2 - 1; j != 0; j--)
		{
			mpz_powm_ui(witpowd.get_mpz_t(), witpowd.get_mpz_t(), 2, candidate.get_mpz_t());
			if (witpowd == 1)
				return false;
			if (witpowd == pminus1)
				break;
		}	

		if (j == 0)
			return false;
	}
	
	return true;
#endif
}

void MillerRabinPrimeGenerator :: PrimeGenIterator :: GeneratePrime()
{
	BIGINTEGER candidate = 0;

	do
	{
		candidate = *++rIter;
	} while (!IsPrime(candidate));

	prime = candidate;
}


// Does a primality test on testprime
bool MillerRabinPrimeGenerator :: PrimeGenIterator :: operator ==(BIGINTEGER testprime)
{
	return IsPrime(testprime);
}

// We implement only prefix
MillerRabinPrimeGenerator :: PrimeGenIterator& MillerRabinPrimeGenerator :: PrimeGenIterator :: operator++() 
{
	GeneratePrime();
	return *this;
}

// Get a prime number from the iterator
BIGINTEGER& MillerRabinPrimeGenerator :: PrimeGenIterator :: operator*()
{
	return prime;
}


