#!/bin/bash
#
#Author: Binesh Andrews <bineshandrews@gmail.com>
#
# Description: Provides a systematic test script to prove the encryption/
# decryption capabilities of the RSA engine and also test its inter-operability
# with OPENSSL.
#
# Last updated on: 9-OCT-2012
#

#Tests 6 combinations
#prog-prog, prog-openssl, openssl-prog each for openssl generated keys and prog generated keys

mkdir -p ../testdata

PROG=../bin/out/rsaeng.out
KEYSIZE=512

openssl genrsa -out ../testdata/private1.pem $KEYSIZE  > /dev/null 2>&1
openssl rsa -in ../testdata/private1.pem -pubout -out ../testdata/public1.pem > /dev/null 2>&1

$PROG genrsa -keysize $KEYSIZE -out ../testdata/private2.pem
$PROG pubkey -in ../testdata/private2.pem -out ../testdata/public2.pem

#print out the keys
echo
echo "Private Key 1:"
$PROG readkey -in ../testdata/private1.pem
echo
echo "Private Key 2:"
openssl rsa -in ../testdata/private2.pem -noout -text
echo
echo "Public Key 1:"
$PROG readkey -in ../testdata/public1.pem
echo
echo "Public Key 2:"
openssl rsa -in ../testdata/public2.pem -pubin -noout -text
echo

#data file
echo "Test message" > ../testdata/test.txt


#Using openssl keys
$PROG encrypt -in ../testdata/test.txt -keyfile ../testdata/public1.pem -out ../testdata/test.enc1
$PROG encrypt -in ../testdata/test.txt -keyfile ../testdata/public2.pem -out ../testdata/test.enc2
openssl rsautl -encrypt -in ../testdata/test.txt -inkey ../testdata/public1.pem -pubin -out ../testdata/test.enc3
openssl rsautl -encrypt -in ../testdata/test.txt -inkey ../testdata/public2.pem -pubin -out ../testdata/test.enc4


$PROG decrypt -in ../testdata/test.enc1 -keyfile ../testdata/private1.pem -out ../testdata/test.dec1
$PROG decrypt -in ../testdata/test.enc2 -keyfile ../testdata/private2.pem -out ../testdata/test.dec2
$PROG decrypt -in ../testdata/test.enc3 -keyfile ../testdata/private1.pem -out ../testdata/test.dec3
$PROG decrypt -in ../testdata/test.enc4 -keyfile ../testdata/private2.pem -out ../testdata/test.dec4

openssl rsautl -decrypt -in ../testdata/test.enc1 -inkey ../testdata/private1.pem -out ../testdata/test.dec5
openssl rsautl -decrypt -in ../testdata/test.enc2 -inkey ../testdata/private2.pem -out ../testdata/test.dec6

cat ../testdata/test.txt ../testdata/test.dec1 ../testdata/test.dec2 ../testdata/test.dec3 ../testdata/test.dec4 ../testdata/test.dec5 ../testdata/test.dec6
sha1sum ../testdata/test.txt ../testdata/test.dec1 ../testdata/test.dec2 ../testdata/test.dec3 ../testdata/test.dec4 ../testdata/test.dec5 ../testdata/test.dec6

