/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Helper routines

Last updated on: 8-OCT-2012 
*******************************************************************************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdint.h>

using namespace std;

void hexdump(uint8_t *data, uint64_t length, int mode);
unsigned char *SHA1Hash(uint8_t *ptr, uint64_t len, uint8_t *digest);
int Sha1File(string filename, uint8_t *out);

#endif
