/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implementation for the random number iterators.

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

#include "random_number.h"

RandNumIterator :: RandNumIterator(uint32_t bitsize) : mr_rstate(gmp_randinit_mt)
{
	randnum = 0;
	mr_bitsize = bitsize;
	// Setup and initialize our random number gen
	// Uses Mersenne Twister for generation
	mr_rstate.seed(time(0) + getpid());
}

RandNumIterator :: RandNumIterator(const RandNumIterator& in) : mr_rstate(gmp_randinit_mt)
{
	mr_bitsize = in.mr_bitsize;
	randnum = in.randnum;
	// Setup and initialize our random number gen
	// Uses Mersenne Twister for generation
	mr_rstate.seed(time(0) + getpid());
}

RandNumIterator& RandNumIterator :: operator++()
{
	randnum = mr_rstate.get_z_bits(mr_bitsize);
	return *this;
}

BIGINTEGER RandNumIterator :: operator*()
{
	return randnum;
}


BIGINTEGER RandNumIterator :: inRange(BIGINTEGER rangemax)
{
	randnum = mr_rstate.get_z_range(rangemax);
	return randnum;
}



MiniRandNumIterator :: MiniRandNumIterator()
{
	randnum = 0;
	seed = time(0) + getpid();
	srand(seed);
}

MiniRandNumIterator :: MiniRandNumIterator(const MiniRandNumIterator& in)
{
	randnum = in.randnum;
	seed = in.seed;
	srand(seed);
}

MiniRandNumIterator& MiniRandNumIterator :: operator++()
{
	
	randnum = rand();
	return *this;
}

uint32_t MiniRandNumIterator :: operator*()
{
	return randnum;
}


uint32_t MiniRandNumIterator :: inRange(uint32_t rangemax)
{
	randnum = rand() % rangemax;
	return randnum;
}

