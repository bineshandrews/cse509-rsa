/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implements the command line shell and parser for the RSA engine. 

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <map>
#include <stdint.h>

#include "utils.h"
#include "random_number.h"
#include "mr_prime_gen.h"
#include "rsa_lib.h"
#include "rsa_x509.h"

using namespace std;

typedef void (*FUNCPTR)();

stringstream inputStream;

map <string, string> params;
map <string, FUNCPTR> cmdHandlers;

void randomNumberHandler(void)
{
	// Requires key size; ignores others.
	uint64_t bitSize;
	stringstream tokenVal(params["-keysize"]);
	
	if (params.find("-keysize") == params.end())
		cout << "Required parameter keysize not specified!" << endl;

	if(!(tokenVal >> bitSize))
		bitSize = 0;

	if (bitSize <= 0)
		cout << "keysize could not be parsed properly."<<endl;
	else
	{
		RandNumIterator rit(bitSize);
		cout << hex << *++rit << endl;
	}
}

void primeNumberHandler(void)
{
	// Requires key size; ignores others.
	uint64_t bitSize;
	stringstream tokenVal(params["-keysize"]);
	
	if (params.find("-keysize") == params.end())
		cout << "Required parameter keysize not specified!" << endl;

	if(!(tokenVal >> bitSize))
		bitSize = 0;

	if (bitSize <= 0)
		cout << "keysize could not be parsed properly."<<endl;
	else if (bitSize < 8)
		cout << "keysize should be > 8."<<endl;
	else
	{
        	MillerRabinPrimeGenerator::PrimeGenIterator pit(bitSize);
		cout << hex << *++pit << endl;
	}
}

void genKeysHandler(void)
{
	// Requires key size; pubkey is optional 
        RSALib rsa;
	BIGINTEGER p, q, n, phi, pubKey = 65537, pvtKey;
	uint64_t bitSize;

	stringstream tokenVal(params["-keysize"]);
	
	if (params.find("-keysize") == params.end())
		cout << "Required parameter keysize not specified!" << endl;

	if(!(tokenVal >> bitSize))
		bitSize = 0;

	if (params.find("-pubkey") != params.end())
		pubKey = BIGINTEGER(params["-pubkey"]);

	if (bitSize <= 0)
		cout << "keysize could not be parsed properly."<<endl;
	else if (bitSize < 128)
		cout << "keysize should be >= 128." << endl;
	else if (pubKey < 10)
		cout << "Please select a bigger public key" << endl;
	else
	{
		rsa.GenerateKeySet(bitSize, &pubKey, &p, &q, &pvtKey);

		if (params.find("-out") != params.end())
		{
			RSAX509::DERParser::WriteKeyFile(params["-out"], &p, &q, &pubKey, &pvtKey);
		}
		else
		{
			n = p * q;
        		//phi = (p - 1) * (q - 1);
        		cout << "P: " << hex << p << endl;
        		cout << "Q: " << hex << q << endl;
			cout << "Modulus (n) : " << hex << n << endl;
        		cout << "Public Key (e): " << hex << pubKey << endl;
        		cout << "Private Key (d): " << hex << pvtKey << endl;
		}
	}
}


void pubKeyHandler(void)
{
	BIGINTEGER p, q, pubKey, pvtKey, n;
	int rc;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	rc = RSAX509::DERParser::ReadKeyFile(params["-in"], &p, &q, &pubKey, &pvtKey);

	if (rc != 0)
	{
		if (rc == -2)
			cout << "Input file doesn't contain a RSA private key" << endl;
		else if (rc == -1)
			cout << "Malformed RSA private key sequence" << endl;
		return;
	}
	n = p * q;

	if (params.find("-out") != params.end())
	{
		RSAX509::DERParser::WritePublicKeyFile(params["-out"], &n, &pubKey);
	}
	else
	{
		cout << "Modulus (n) : " << hex << n << endl;
       		cout << "Public Key (e): " << hex << pubKey << endl;
	}
}


void encryptHandler(void)
{
	BIGINTEGER pubKey, modulus;
        RSALib rsa;
	int rc;
	char buffer[4096];
	char encOutBuf[4096];
	uint64_t blocksize, encLen;
	size_t readsize;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	if (params.find("-out") == params.end()) 
	{
		cout << "Please provide the file to write output to." << endl;
		return;
	}

	if (params.find("-keyfile") == params.end()) 
	{
		cout << "Please provide the RSA key file." << endl;
		return;
	}

	rc = RSAX509::DERParser::ReadPublicKeyFile(params["-keyfile"], &modulus, &pubKey);

	if (rc != 0)
	{
		cout << "Failed to open key file." << endl;
		return;
	}

	blocksize = RSALib::MaxMsgChunkSize(modulus);

	if (blocksize == 0) 
	{
		cout << "Keys are not appropriate for encryption with PKCS padding" << endl;
		return;
	}

	ifstream inFile(params["-in"].c_str(), ios::binary);

	if (!inFile)	
	{
		cout << "Failed to open source file." << endl;
		return;
	}

	ofstream outFile(params["-out"].c_str(), ios::binary);

	if (!inFile)	
	{
		cout << "Failed to open destination file." << endl;
		return;
	}

	while(inFile)
	{
		inFile.read(buffer, blocksize);
		readsize = inFile.gcount();
		if (readsize == 0)
			break;
		rsa.Encrypt((uint8_t *)buffer, readsize, pubKey, modulus, (uint8_t *)encOutBuf, &encLen);
		outFile.write(encOutBuf, encLen); 
	}

	inFile.close();
	outFile.close();
}

void decryptHandler(void)
{
	BIGINTEGER p, q, pubKey, pvtKey, modulus;
        RSALib rsa;
	int rc;
	char buffer[4096];
	char outBuf[4096];
	uint64_t blocksize, decLen;
	size_t readsize;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	if (params.find("-out") == params.end()) 
	{
		cout << "Please provide the file to write output to." << endl;
		return;
	}

	if (params.find("-keyfile") == params.end()) 
	{
		cout << "Please provide the RSA key file." << endl;
		return;
	}

	rc = RSAX509::DERParser::ReadKeyFile(params["-keyfile"], &p, &q, &pubKey, &pvtKey);

	if (rc != 0)
	{
		cout << "Failed to open key file." << endl;
		return;
	}

	modulus = p * q;
	blocksize = RSALib::MaxChunkSize(modulus);

	if (blocksize == 0) 
	{
		cout << "Keys are invalid" << endl;
		return;
	}

	ifstream inFile(params["-in"].c_str(), ios::binary);

	if (!inFile)	
	{
		cout << "Failed to open source file." << endl;
		return;
	}

	ofstream outFile(params["-out"].c_str(), ios::binary);

	if (!inFile)	
	{
		cout << "Failed to open destination file." << endl;
		return;
	}

	rc = 0;
	while(inFile)
	{
		inFile.read(buffer, blocksize);
		readsize = inFile.gcount();

		if (readsize == 0)
			break;

		if (readsize != blocksize)
		{
			rc = -1;
			break;
		}
		rc = rsa.Decrypt((uint8_t *)buffer, readsize, pvtKey, modulus, (uint8_t *)outBuf, &decLen);
		if (rc != 0)
			break;
		outFile.write(outBuf, decLen); 
	}

	inFile.close();
	outFile.close();

	if (rc != 0)
		cout << "Decryption failed" << endl;
}

void readKeyHandler(void)
{
	BIGINTEGER p, q, pubKey, pvtKey, n;
	int rc;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	rc = RSAX509::DERParser::ReadKeyFile(params["-in"], &p, &q, &pubKey, &pvtKey);

	if (rc == 0)
	{
		n = p * q;
		cout << "P: " << hex << p << endl;
		cout << "Q: " << hex << q << endl;
		cout << "Modulus: " << hex << n << endl;
		cout << "Private Key: " << hex << pvtKey << endl;
		cout << "Public Key: " << hex << pubKey << endl;
	}

	else if (rc == -2)
	{
		// The input file didn't contain a private key;
		// Check whether it contains a public key..
		rc = RSAX509::DERParser::ReadPublicKeyFile(params["-in"], &n, &pubKey);

		if (rc == 0)
		{
			cout << "Modulus: " << hex << n << endl;
			cout << "Public Key: " << hex << pubKey << endl;
		}
	}
}

void readCertHandler(void)
{
	string encoded, decoded;
	int64_t len;
	uint8_t buffer[2048];

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read cert info from." << endl;
		return;
	}

	RSAX509::ReadPEMBlockFromFile(params["-in"].c_str(), MARKER_CERT, encoded);

	if (!encoded.empty())
	{
		cout << encoded << endl << endl;

		// Decode the BASE64 format
		len = RSAX509::Base64Decode(encoded, buffer);	

		cout << "Decoded key octects: " << endl;
		hexdump(buffer, len, 1);
		cout << endl;
	}
}

void writeCertHandler(void)
{
	string encoded, decoded;
	int64_t len;
	uint8_t buffer[2048];
	RandNumIterator rit(64);

	BIGINTEGER seqNum = *++rit;

	if (params.find("-out") == params.end()) 
	{
		cout << "Please provide the file to write cert into to." << endl;
		return;
	}

	if (params.find("-keyfile") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	if (RSAX509::DERParser::WriteCert(params["-out"], params["-keyfile"], seqNum) == 0)
	{
		cout << "Self signed certificate written to file: " << params["-out"] << endl;
	}
}


void verifySignHandler()
{
	string subject, issuer;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to read certificate info from." << endl;
		return;
	}

	int ret = RSAX509::DERParser::CertVerifySign(params["-in"], subject, issuer);

		// No file provided. So we are just doing a self signing check.
	if (ret == 0)
		cout << "Certificate sign verified OK! [subject: " << subject << ", issuer: " << issuer <<" ]" << endl;
	else if (ret == -2)
		cout << "Certificate validity expired!" << endl;
	else if (ret == -3)
		cout << "Certificate is not self signed - Unable to verify!" << endl;
	else 
		cout << "Certificate failed sign verification! [subject: " << subject << ", issuer: " << issuer <<" ]" << endl;
}

void signFileHandler(void)
{
	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to sign." << endl;
		return;
	}

	if (params.find("-keyfile") == params.end()) 
	{
		cout << "Please provide the file to read key info from." << endl;
		return;
	}

	if (params.find("-out") == params.end()) 
	{
		cout << "Please provide the file to write certificate to." << endl;
		return;
	}

	if (params.find("-signature") == params.end()) 
	{
		cout << "Please provide the file to write signature to." << endl;
		return;
	}

	if (RSAX509::DERParser::SignFileHash(params["-in"], params["-keyfile"], params["-signature"], params["-out"]) == 0)
	{
		cout << "Self signed certificate written to file: " << params["-out"] << endl;
		cout << "Signed hash written to file: " << params["-signature"] << endl;
	}
}

void verifyFileHandler(void)
{
	int rc;
	BIGINTEGER n, pubKey;

	if (params.find("-in") == params.end()) 
	{
		cout << "Please provide the file to sign." << endl;
		return;
	}

	if (params.find("-keyfile") == params.end() && params.find("-certfile") == params.end()) 
	{
		// Either one should be present
		cout << "Please provide the file to read public key info from." << endl;
		return;
	}

	if (params.find("-signature") == params.end()) 
	{
		cout << "Please provide the file to read signature from." << endl;
		return;
	}

	if (params.find("-keyfile") != params.end())
		rc = RSAX509::DERParser::VerifyFileHashKeyFile(params["-in"], params["-signature"], params["-keyfile"]); 
	else
		rc = RSAX509::DERParser::VerifyFileHashCert(params["-in"], params["-signature"], params["-certfile"]);

	if (rc == 0)
		cout << "Hash signature verified" << endl;
	else
		cout << "Signature could not be verified" << endl;

}

void helpHandler(void)
{
	cout << "Supported commands are: " << endl;
	cout << "\t#genrandom -keysize <size>" << endl;
	cout << "\t#genprime -keysize <size>" << endl;
	cout << "\t#genrsa -keysize <size> [-out <private key file>]" << endl;
	cout << "\t#pubkey -in <private key file> [-out <public key file>]" << endl;
	cout << "\t#readkey -in <key file>" << endl;
	cout << "\t#readcert -in <cert file>" << endl;
	cout << "\t#verifycert -in <cert file>" << endl;
	cout << "\t#writecert -keyfile <private key file> -out <cert file>" << endl;
	cout << "\t#encrypt -in <inp file> -keyfile <pub key file> -out <enc file>" << endl;
	cout << "\t#decrypt -in <enc file> -keyfile <pvt key file> -out <dec file>" << endl;
	cout << "\t#signfile -in <inp file> -keyfile <pvt> -signature <hashfile> -out <certfile>" << endl;
	cout << "\t#verifyfile -in <inp> [{-keyfile <pvt>}{-certfile <cert>}] -signature <hashfile>" << endl;
}

void exitHandler(void)
{
	exit(0);
}

void inputStreamHandler(void)
{
	string token1, token2, token3;
	char delimiter = ' ';
	FUNCPTR handler = NULL;

	params.clear();

	if(getline(inputStream, token1, delimiter))
	{
		while (inputStream.peek() == delimiter)
			inputStream.get();

		handler = cmdHandlers[token1];
	}

	if (!handler)
	{
		cout << "Unrecognized command: " << token1 << endl;
		return;
	}

	while(getline(inputStream, token1, delimiter))
	{
		while (inputStream.peek() == delimiter)
			inputStream.get();

	 	// 
		// TODO: Put these elements in some collection
		if (token1.compare("-keysize") == 0 ||
		    token1.compare("-msg") == 0 ||
		    token1.compare("-pubkeyfile") == 0 ||
		    token1.compare("-in") == 0 ||
		    token1.compare("-out") == 0 ||
		    token1.compare("-signature") == 0 ||
		    token1.compare("-certfile") == 0 ||
		    token1.compare("-keyfile") == 0)
		{
			//Keysize integer follows
			if(getline(inputStream, token2, delimiter))
			{
			
				while (inputStream.peek() == delimiter)
					inputStream.get();

				params[token1] = token2;
			}
			else
			{
				cout << "Malformed input!" << endl;
				return;
			}
		}
	}
		

#if 0
	for (map<string, string> :: iterator it = params.begin(); it != params.end(); it++)
		cout << it->first << " : " << it->second << endl;
#endif

	(*handler)();
}

void initCmdHandler(void)
{
	/* Add commands here */
	/* Each individual handler is responsible to check for args conformance */
	cmdHandlers["genrandom"] = randomNumberHandler;
	cmdHandlers["genprime"] = primeNumberHandler;
	cmdHandlers["genrsa"] = genKeysHandler;
	cmdHandlers["pubkey"] = pubKeyHandler;
	cmdHandlers["encrypt"] = encryptHandler;
	cmdHandlers["decrypt"] = decryptHandler;
	cmdHandlers["readkey"] = readKeyHandler;
	cmdHandlers["readcert"] = readCertHandler;
	cmdHandlers["writecert"] = writeCertHandler;
	cmdHandlers["verifysign"] = verifySignHandler;
	cmdHandlers["signfile"] = signFileHandler;
	cmdHandlers["verifyfile"] = verifyFileHandler;
	cmdHandlers["help"] = helpHandler;
	cmdHandlers["quit"] = exitHandler;
	cmdHandlers["exit"] = exitHandler;
	cmdHandlers["halt"] = exitHandler;
}

int main(int argc, char *argv[])
{
	string inputLine;

	initCmdHandler();

	if (argc == 1)
	{
		while(1)
		{
			cout << "RSAEngine# " << flush;
			getline(cin, inputLine, '\n');
			if (!inputLine.empty())
			{
				inputStream.str(inputLine);
				inputStreamHandler();
				inputStream.clear();
			}
		}
	}
	else
	{
		inputStream.str("");
		for (size_t i = 1; i < argc; i++)
			inputStream << argv[i] << " ";
		inputStreamHandler();
	}
	return 0;
}
