/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Wrappers for the big integer library. 

Last updated on: 8-OCT-2012
*******************************************************************************/

#ifndef __BIGINTEGER_H__
#define __BIGINTEGER_H__

#include <gmp.h>
#include <gmpxx.h>

#define BIGINTEGER mpz_class 

//TODO: Wrap all explicit functions

#endif
