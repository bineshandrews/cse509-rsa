/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implements the core RSA functions of generating primes, encryption
decryption, etc as well as the helper routines to convert BIGINT to bit string
and vice versa. The basic encryption and decryption routines are extended to 
allow encryption and decryption of files of any size. 

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iomanip>
#include <iostream>
#include <sstream>
#include <assert.h>

#include "utils.h"
#include "rsa_lib.h"
#include "random_number.h"
#include "mr_prime_gen.h"

using namespace std;

STATUS RSALib :: FindMultiplicativeInvMod(BIGINTEGER *phi, BIGINTEGER *e, BIGINTEGER *d)
{
	// d * e = 1 mod phi; d = e^-1 * 1 + y*phi ==> e * x = 1 + y*phi
	// ==> e * x - phi * y = 1 = gcd(e, phi)
	// Use the extended euclidean algorithm to solve for x and y and substitute.

	BIGINTEGER x = 0, y =1, x1 = 1, y1 = 0;
	BIGINTEGER a = *e, b = *phi, t1, t2;

	while (b != 0)
	{
		t1 = a / b;

		t2 = a;
		a = b;
		b = t2 % b;

		t2 = x;
		x = x1 - t1 * x;
		x1 = t2;

		t2 = y;
		y = y1 - t1 * y;
		y1 = t2;
	}

	// x1 and y1 contains the values.
	// Now calculate (1 + y * phi) / (x * e) = d

	// If x < 0 add, b (because : a(x + b) + b (y -a) = ax + by itself.
	*d = x1 > 0 ? x1 : x1 + *phi;
	// y1 = y1 - *e in this case

	// clean up; x1 and y1 contains the values.
	x = 0;
	y = 0;
	x1 = 0;
	y1 = 0;
	a = 0;
	b = 0;
	t1 = 0;
	t2 = 0;	
	return OK;
}

BIGINTEGER RSALib :: MsgIntRep(uint8_t *msg, uint32_t len)
{
	stringstream hexrep("");
	for (size_t i; i < len; i++)
		hexrep << setfill('0') << setw(2) << hex << (int)msg[i]; 

	BIGINTEGER ret;
	mpz_set_str(ret.get_mpz_t(), hexrep.str().c_str(), 16);
	return ret;
}

//len is the desired length
uint64_t RSALib :: MsgStrRep(BIGINTEGER intrep, uint8_t *str, uint64_t len, bool prepZero)
{
	stringstream hexrep("");
	hexrep << hex << intrep; 
	char buffer[8192 + 1];
	size_t length = hexrep.str().length();
	int prepZeroed = 0;

	memcpy(buffer, hexrep.str().c_str(), hexrep.str().length());
	//const char *ptr = hexrep.str().c_str();
	const char *ptr = buffer;
	uint64_t num;
	char numstr[3] = {0};
	ssize_t zero_run = len - (length + 1)/2;

	assert (zero_run >= 0);

	// This is for the DER encoding use. If the highest bit >= 0x80
	// I.e. looks like a negative number in INT format, prepend a 0
	if (zero_run == 0 && prepZero && length % 2 == 0 && ptr[0] > '7')
		zero_run++, prepZeroed=1;

	while (zero_run)
	{
		*(str++) = 0;
		zero_run--;
	}

	if(length % 2)
	{
		numstr[0] = *(ptr++);
		numstr[1] = 0;
		sscanf(numstr, "%lx", &num);
		length -= 1;
	}
	else
	{
		numstr[0] = *(ptr++);
		numstr[1] = *(ptr++);
		sscanf(numstr, "%lx", &num);
		length -= 2;
	}

	*(str++) = (uint8_t)num;

	while(length)
	{
		numstr[0] = *(ptr++);
		numstr[1] = *(ptr++);
		sscanf(numstr, "%lx", &num);
		*(str++) = (uint8_t)num;
		length -= 2;

	}
	
	//*str = 0;
	return (len + prepZeroed);
}

STATUS RSALib :: CalculatePrivateKey(BIGINTEGER *phi, BIGINTEGER *pubKey, BIGINTEGER *pvtKey)
{
	// First let us fix the public key!
	// We need a number co-prime to phi
	FindMultiplicativeInvMod(phi, pubKey, pvtKey);
	return OK;
}

void RSALib :: GenerateKeySet(uint64_t bitSize, BIGINTEGER *pubKey, BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pvtKey)
{
        MillerRabinPrimeGenerator::PrimeGenIterator i(bitSize/2);

	// Generate 2 primes, p and q
        *p = *++i; 
	*q = *++i;

        // Calculate n = p * q and phi = (p-1)*(q-1);
	BIGINTEGER n = (*p) * (*q);
        BIGINTEGER phi = ((*p) - 1) * ((*q) - 1);

	// Use extended euclidean algorithm to calculate the private key
        CalculatePrivateKey(&phi, pubKey, pvtKey);
}

size_t RSALib :: MaxChunkSize(BIGINTEGER modulus)
{
	// K
	return ((1 + mpz_sizeinbase(modulus.get_mpz_t(), 16)) / 2); 
}

size_t RSALib :: MaxMsgChunkSize(BIGINTEGER modulus)
{
	// K - 11
	size_t sz = MaxChunkSize(modulus);

	return sz > PKCS_PAD_OVERHEAD ? (sz - PKCS_PAD_OVERHEAD) : 0;
}


// Size of encrypted message = chunksize
// We use the PKCS padding scheme
STATUS RSALib :: EncryptChunk(uint8_t *msg, uint64_t msgLen, BIGINTEGER key, BIGINTEGER modulus, uint8_t *encMsg, uint8_t type)
{
	uint8_t padbytes[256] = {0x00, 0x02};
	MiniRandNumIterator rit;

	size_t k = MaxChunkSize(modulus); 
	if (k < 12)
		assert(false);

	size_t msg_block_size = k - PKCS_PAD_OVERHEAD;

	if (type)
		padbytes[1] = 0x01;

	// PKCS padding: 3 fixed bytes and remaining random
	size_t i, padding = k - 3 - ( msg_block_size < msgLen ? msg_block_size : msgLen);
	uint32_t randword = 0;

	for (i = 0; i < padding; i++)
	{
		if (!type)
		{
			while(!(randword & 0xFF))
				randword = *++rit;

			padbytes[2+i] = randword & 0xFF;
			randword >>= 8;
		}
		else
			padbytes[2+i] = 0xff;
	}

	padbytes[2 + i] = 0x00;

	//hexdump(padbytes, 3+i, 1);
	// Message comes after this;

	BIGINTEGER padInt = MsgIntRep(padbytes, i + 3);
	BIGINTEGER msgInt = MsgIntRep(msg, msgLen);
	BIGINTEGER pow256, encMsgInt;
	mpz_ui_pow_ui(pow256.get_mpz_t(), 256, msgLen);
	msgInt = msgInt + padInt * pow256;

	//cout << "Padded message: " << hex << msgInt << endl;

	mpz_powm_sec(encMsgInt.get_mpz_t(), msgInt.get_mpz_t(), key.get_mpz_t(), modulus.get_mpz_t());
	//cout << "Encrypted message: " << hex << encMsgInt << endl;
	MsgStrRep(encMsgInt, encMsg, MaxChunkSize(modulus), false);

	return OK;
}

STATUS RSALib :: DecryptChunk(uint8_t *encMsg, BIGINTEGER key, BIGINTEGER modulus, uint8_t *msg, uint64_t *msgLen, uint8_t type)
{
	uint8_t *tmp;
	uint64_t count = 0, chunkSize = MaxChunkSize(modulus);
	BIGINTEGER unEncInt;
	BIGINTEGER encInt = MsgIntRep(encMsg, MaxChunkSize(modulus));

	//cout << hex << encInt << endl;
	mpz_powm_sec(unEncInt.get_mpz_t(), encInt.get_mpz_t(), key.get_mpz_t(), modulus.get_mpz_t());
	//cout << hex << unEncInt << endl;
	MsgStrRep(unEncInt, msg, chunkSize, false); 
	//hexdump(msg, chunkSize, 1);
	// Note that this include padding bytes now and needs to be stripped
	// The message starts after the second 00 in the padding.
	if (msg[0] != 0)
		return ERROR;
	if (type && msg[1] != 0x01)
		return ERROR;
	else if (!type && msg[1] != 0x02)
		return ERROR;

	for(tmp = msg + 2; *tmp && count < chunkSize; tmp++, count++)
	{
		// Signing - padding bytes should be 0xff
		if(type && *tmp != 0xff)
			return ERROR;
	}
	
	if(*tmp || (tmp - msg + 1) < PKCS_PAD_OVERHEAD) {
		*msgLen = 0;
		return ERROR;
	}

	*msgLen = chunkSize - (tmp - msg + 1);

	if(*msgLen > (chunkSize - PKCS_PAD_OVERHEAD))
	{
		*msgLen = 0;
		return ERROR;
	}

	memmove(msg, tmp + 1, *msgLen);
	msg = tmp + 1;
	return OK;
}

STATUS RSALib :: Encrypt(uint8_t *msg, uint64_t msgLen, BIGINTEGER key, BIGINTEGER modulus, uint8_t *encMsg, uint64_t *encMsgLen, uint8_t type)
{
	size_t k = MaxChunkSize(modulus); 
	if (k < 12)
		assert(false);

	size_t msg_block_size = k - PKCS_PAD_OVERHEAD;
	uint8_t *ptr = msg;
	STATUS rc = OK;

	*encMsgLen = 0;
	for(int64_t remLen = msgLen; remLen > 0 ; ptr += msg_block_size, remLen -= msg_block_size, encMsg += k)
	{
		rc = EncryptChunk(ptr, remLen > msg_block_size ? msg_block_size : remLen, key, modulus, encMsg, type); 
		if (rc != OK)
			break;
		*encMsgLen += k;
	}

	return rc;
}

STATUS RSALib :: Decrypt(uint8_t *encMsg, uint64_t encMsgSize, BIGINTEGER key, BIGINTEGER modulus, uint8_t *msg, uint64_t *msgLen, uint8_t type)
{

	size_t k = MaxChunkSize(modulus); 
	if (k < 12)
		assert(false);

	if(encMsgSize % k != 0)
		return ERROR;

	uint8_t *ptr = encMsg;
	uint64_t numChunks = encMsgSize / k;
	uint64_t cMsgLen;
	STATUS rc = OK;

	*msgLen = 0;
	for (size_t i = 0; i < numChunks; i++, ptr+=k)
	{
		rc = DecryptChunk(ptr, key, modulus, msg + *msgLen, &cMsgLen, type);
		if (rc != OK)
			break;
		*msgLen += cMsgLen;
	}

	return rc;
	
}
