/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: X.509 and DER related functionality.

Last updated on: 8-OCT-2012 
*******************************************************************************/

#ifndef __RSA_X509_H__
#define __RSA_X509_H__

#define MARKER_CERT_PVT_KEY "PRIVATE KEY"
#define MARKER_CERT "CERTIFICATE"
#define MARKER_PVT_KEY "RSA PRIVATE KEY"
#define MARKER_PUB_KEY "PUBLIC KEY"
#define MARKER_CERTIFICATE "CERTIFICATE"

using namespace std;

class RSAX509
{
	public:
		static void Base64Encode(uint8_t *input, uint64_t ipLen, string &encodedStr);
		static int64_t Base64Decode(string &input, uint8_t *decoded);
		static int64_t ReadPEMBlockFromFile(const string &filename, const string &marker, string &encodedBlock);
		static int64_t WritePEMBlockToFile(const string &filename, const string &marker, const string &encodedBlock);

		class DERParser
		{

			public:
			// Delete Parse later
			static int Parse(uint8_t *ptr, uint64_t len);
			static size_t ParseTag(uint8_t *ptr, uint64_t len, uint64_t *tag);
			static size_t ParseLength(uint8_t *ptr, uint64_t len, uint64_t *length);
			static size_t ParseInt(uint8_t *ptr, uint64_t len, BIGINTEGER *val);
			// Parse a bit string as a big integer
			static size_t ParseBitStringInt(uint8_t *ptr, uint64_t len, BIGINTEGER *val);
			static size_t ParseBitString(uint8_t *ptr, uint64_t len, uint8_t **bits, uint64_t *size);
			static size_t ParseSequence(uint8_t *ptr, uint64_t len, uint64_t *seqLen);
			static size_t ParseGeneral(uint8_t *ptr, uint64_t len, uint64_t *tag, uint64_t *attrLen);
			static size_t EncodeTag(uint64_t tag, uint8_t highbits, uint8_t *buffer);
			static size_t EncodeLength(uint64_t length, uint8_t *buffer);
			static size_t EncodeBigInt(BIGINTEGER *val, uint64_t tag, uint8_t *buffer);
			static void EncodePrivateKey (BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pubK, BIGINTEGER *pvtK, uint8_t *buffer, uint64_t *len, uint64_t *off);
			static void EncodePublicKey (BIGINTEGER *modulus, BIGINTEGER *pubK, uint8_t *buffer, uint64_t *len, uint64_t *off);
			static int WriteKeyFile(string filename, BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pubK, BIGINTEGER *pvtK);
			static int WritePublicKeyFile(string filename, BIGINTEGER *modulus, BIGINTEGER *pubK);
			static int ReadKeyFile(string filename, BIGINTEGER *pr1, BIGINTEGER *pr2, BIGINTEGER *pubK, BIGINTEGER *pvtK);
			static int ParsePublicKey(uint8_t *ptr, uint64_t len, BIGINTEGER *modulus, BIGINTEGER *pubK);
			static int ReadPublicKeyFile(string filename, BIGINTEGER *modulus, BIGINTEGER *pubK);
			static int ParseSubject(uint8_t *p, uint64_t len1, string &subject);
			static int ParseCertValid(uint8_t *cert, uint64_t len, bool *timeValid, string &subject, string &signer, BIGINTEGER *modulus, BIGINTEGER *pubKey);
			static int CertGetPubKey(string filename, BIGINTEGER *modulus, BIGINTEGER *pubKey);
			static int CertVerifySign(string filename, string &subject, string &signer);
			static int WriteCert(string filename, string keyfile, BIGINTEGER seqNo);
			static int SignFileHash(string filename, string keyfile, string signfile, string outfile);
			static int VerifyFileHash(string filename, string hashfilename, BIGINTEGER *modulus, BIGINTEGER *pubK);
			static int VerifyFileHashCert(string filename, string hashfilename, string certfile);
			static int VerifyFileHashKeyFile(string filename, string hashfilename, string pubKeyFile);
		};
		
};

#endif
