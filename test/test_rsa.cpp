#include <iostream>
#include <iomanip>
#include <assert.h>
#include <stdint.h>

#include "gmpxx.h"

#include "utils.h"
#include "mr_prime_gen.h"
#include "rsa_lib.h"
#include "rsa_x509.h"

BIGINTEGER n, pubKey, pvtKey;
uint8_t encTestMsg[4096];
uint8_t decTestMsg[4096];

void test_MsgEncryption()
{
	char *teststr = (char *)"~(abcdefghijklmnopqrstuvwxyz_0123456789_ABCDEFGHIJKLMNOPQRSTUVWXYZ)!";
	//char *encteststr = (char *)"abcdefg";
	char *encteststr = teststr;
	char outstr[256] ="";
	BIGINTEGER intrep;
	uint64_t length;
	uint64_t encMsgLen, decMsgLen;
        RSALib rsa;

	cout << endl << endl << "[Testing basic message encryption/decryption]"<<endl;
	intrep = RSALib::MsgIntRep((uint8_t *)"zzzz", 4);
	assert (intrep == 2054847098);

	intrep = RSALib::MsgIntRep((uint8_t *)teststr, strlen(teststr));
	RSALib::MsgStrRep(intrep, (uint8_t *)outstr, strlen(teststr), false);
	//cout << outstr << " " << teststr <<  " " << hex << intrep << endl;
	assert(strcmp(teststr, outstr) == 0);
	assert (intrep == RSALib::MsgIntRep((uint8_t *)teststr, strlen(teststr)));

	cout << endl << endl;
	cout << "Plain text message : " << endl;
	hexdump((uint8_t *)encteststr, strlen(encteststr), 1);

	rsa.Encrypt((uint8_t *)encteststr, strlen(encteststr), pvtKey, n, encTestMsg, &encMsgLen);

	cout << endl << endl;
	cout << "Encrypted message : " << endl;
	hexdump(encTestMsg, encMsgLen, 1);
	assert(rsa.Decrypt(encTestMsg, encMsgLen, pubKey, n, decTestMsg, &decMsgLen) == OK);
	assert(decMsgLen == strlen(teststr));
	assert(memcmp(decTestMsg, teststr, decMsgLen) == 0);

	cout << endl << endl;
	cout << "Decrypted message : " << endl;
	hexdump(decTestMsg, decMsgLen, 1);
	cout << endl << endl;
}

void test_Base64Encoding()
{
	string encoded, decoded;
	int64_t len;
	uint8_t buffer[2048];
	string filename1 = "./certs/mycert.pem";
	string filename2 = "./certs/mycert1.pem";

	cout << endl << endl << "[Testing Base64 encoding and decoding from "<< filename1 << " ]" << endl;
	// Read the provate key from the file
	//RSAX509::ReadPEMBlockFromFile("./certs/mycert.pem", MARKER_PVT_KEY, encoded);
	RSAX509::ReadPEMBlockFromFile(filename1.c_str(), MARKER_CERT, encoded);
	RSAX509::WritePEMBlockToFile(filename2.c_str(), MARKER_CERT, encoded);
	encoded = "";
	RSAX509::ReadPEMBlockFromFile(filename2.c_str(), MARKER_CERT, encoded);
#if 0
	cout << "Private key in BASE64 format : " << endl;
	hexdump((uint8_t *)encoded.c_str(), encoded.length(), 1);
	cout << endl << endl;
#endif
	
	// Decode the BASE64 format
	len = RSAX509::Base64Decode(encoded, buffer);	

#if 0 
	cout << "Decoded key octects: " << endl;
	hexdump(buffer, len, 1);
	cout << endl << endl;
#endif

	// Encode back
	RSAX509::Base64Encode(buffer, len, decoded);

#if 0 
	cout << "Encoded back to BASE64 format : " << endl;
	hexdump((uint8_t *)decoded.c_str(), decoded.length(), 1);
	cout << endl << endl;
#endif

	assert(encoded.compare(decoded) == 0);
        cout << "[OK]" << endl << endl;
}

void test_ASNEncoding()
{
	string encoded, decoded;
	int64_t len;
	uint8_t buffer[2048];
	string filename1 = "./certs/mykey.pem";

	cout << endl << endl << "[Testing ASN encoding from "<< filename1 << " ]" << endl;
	// Read the provate key from the file
	RSAX509::ReadPEMBlockFromFile(filename1.c_str(), MARKER_PVT_KEY, encoded);

	cout << encoded << endl << endl;

	// Decode the BASE64 format
	len = RSAX509::Base64Decode(encoded, buffer);	

#if 1 
	cout << "Decoded key octects: " << endl;
	hexdump(buffer, len, 1);
	cout << endl << endl;
#endif

	RSAX509::DERParser :: Parse(buffer, len);
}

void test_PrimeNumberGenerator()
{
        // Test the prime number generating iterator.
	cout << endl << endl << "[Testing prime number generation]"<<endl;
        MillerRabinPrimeGenerator::PrimeGenIterator i(512);
        MillerRabinPrimeGenerator::PrimeGenIterator i2(512);
        mpz_class someprime;

        for (int j = 0; j < 10; j++)
        {
                
                cout << (*++i) << endl;
        }

        someprime = *++i2;
        assert(i == someprime); // since someprime is prime, this should succeed!
        cout << endl;
}

void test_PrivateKeyGen()
{
	cout << endl << endl << "[Testing key generation]"<<endl;

        RSALib rsa;
	BIGINTEGER p, q, phi, temp;
        MillerRabinPrimeGenerator::PrimeGenIterator i(256);
	uint8_t buffer[2048];
	uint64_t len, off;
	string filename = "./certs/testkey.pem";

        // Set public key = 65537 (=F5 fermat number; prime and 2^x + 1 form)
        // (2^(2^x)) infact) and find private key
	pubKey = 65537;

	rsa.GenerateKeySet(1024, &pubKey, &p, &q, &pvtKey);
	n = p * q;
        phi = (p - 1) * (q - 1);

        temp = pubKey * pvtKey;
        temp %= phi;
        assert ((temp % phi) == 1);
        cout << "P: " << hex << p << endl;
        cout << "Q: " << hex << q << endl;
	cout << "Modulus: " << hex << n << endl;
        cout << "Public Key: " << hex << pubKey << endl;
        cout << "Private Key: " << hex << pvtKey << endl;
        cout << "[OK]" << endl;

	RSAX509::DERParser :: EncodePrivateKey(&p, &q, &pubKey, &pvtKey, buffer, &len, &off);
	cout << "Parsing .. " << len << " " << off << " " << (int)*buffer << endl;
	RSAX509::DERParser :: Parse(buffer+off, len);
	RSAX509::DERParser :: WriteKeyFile(filename, &p, &q, &pubKey, &pvtKey);

	// Test one conceptual encryption and decryption.
	BIGINTEGER plaintext = *++i;
	BIGINTEGER encrypted, decrypted;
	mpz_powm_sec(encrypted.get_mpz_t(), plaintext.get_mpz_t(), pubKey.get_mpz_t(), n.get_mpz_t());
	mpz_powm_sec(decrypted.get_mpz_t(), encrypted.get_mpz_t(), pvtKey.get_mpz_t(), n.get_mpz_t());
	assert(plaintext == decrypted);	
}

int main(void)
{
        //test_PrimeNumberGenerator();    
        test_PrivateKeyGen();
	test_MsgEncryption();
	test_Base64Encoding();
	test_ASNEncoding();
        return 0;
}
