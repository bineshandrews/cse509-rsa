/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implements the necessary functions to parse and write to openssl
compatible files. This includes BASE64 encode and decode, DER encoding for
private key files, public key files, and certificates and signed hashes. 
Additional utilities to verify a certificate sign, sign a certificate, to sign
a file hash, to verify the signed file hash, etc are also provided.

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdint.h>
#include <assert.h>
#include <ctime>

#include "biginteger.h"
#include "random_number.h"
#include "rsa_x509.h"
#include "rsa_lib.h"
#include "utils.h"

using namespace std;

static const unsigned char base64_enc_map[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const unsigned char base64_dec_map[128] =
{
    127, 127, 127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127,  62, 127, 127, 127,  63,  52,  53,
     54,  55,  56,  57,  58,  59,  60,  61, 127, 127,
    127,  64, 127, 127, 127,   0,   1,   2,   3,   4,
      5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,
     25, 127, 127, 127, 127, 127, 127,  26,  27,  28,
     29,  30,  31,  32,  33,  34,  35,  36,  37,  38,
     39,  40,  41,  42,  43,  44,  45,  46,  47,  48,
     49,  50,  51, 127, 127, 127, 127, 127
};

// Returns length of encoded string.
// == if last block encodes 1 octect
// = if last block encodes 2 octects
void RSAX509 :: Base64Encode(uint8_t *input, uint64_t ipLen, string &encodedStr)
{
	uint64_t numGrps = ipLen / 3;
	uint64_t linecount = 0;
	stringstream encoded;
	char en[5] = { 0 };
	int val;
	uint8_t *ptr = input;

	for (size_t i = 0; i < numGrps; i++)
	{
		if (linecount == 64) {
			encoded << "\n";
			linecount = 0;
		}

		val = ((*ptr) >> 2) & 0x3F;
		en[0] = base64_enc_map[val];
		val = ((*ptr) & 0x03) << 4;
		ptr++;
		val |= ((*ptr) >> 4) & 0x0F;
		en[1] = base64_enc_map[val];
		val = ((*ptr) & 0x0F) << 2;
		ptr++;
		val |= ((*ptr) >> 6) & 0x03;
		en[2] = base64_enc_map[val];
		val = (*ptr) & 0x3F;
		en[3] = base64_enc_map[val];
		*ptr++;

		encoded << en;
		linecount += 4;
	} 

	if (ipLen % 3)
	{
		if (linecount == 64)
			encoded << "\n";

		val = ((*ptr) >> 2) & 0x3F;
		en[0] = base64_enc_map[val];
		switch (ipLen % 3)
		{
			case 1:
				val = ((*ptr) & 0x03) << 4;
				en[1] = base64_enc_map[val];
				en[2] = '=';
				en[3] = '=';
				break;
			case 2:
				val = ((*ptr) & 0x03) << 4;
				ptr++;
				val |= ((*ptr) >> 4) & 0x0F;
				en[1] = base64_enc_map[val];
				val = ((*ptr) & 0x0F) << 2;
				en[2] = base64_enc_map[val];
				en[3] = '=';
				break;
			case 0:
				break;
		}

		encoded << en;
		linecount += (ipLen % 3);
	}

	encodedStr = encoded.str();
}

int64_t RSAX509 :: Base64Decode(string &input, uint8_t *decoded)
{
	unsigned char enc[4] = {0};
	const char *cptr = input.c_str();
	uint64_t length = input.length();
	uint64_t octects = 0;
	uint64_t ws = 0, i;

	while (*cptr == '\n' || *cptr == '\r' || *cptr == '\0') cptr++, ws++;

	for(i = 0; i < length; i+=4)
	{
		enc[0] = base64_dec_map[*(cptr++)];
		while (*cptr == '\n' || *cptr == '\r' || *cptr == '\0') cptr++, i++, ws++;
		enc[1] = base64_dec_map[*(cptr++)];
		while (*cptr == '\n' || *cptr == '\r' || *cptr == '\0') cptr++, i++, ws++;
		enc[2] = base64_dec_map[*(cptr++)];
		while (*cptr == '\n' || *cptr == '\r' || *cptr == '\0') cptr++, i++, ws++;
		enc[3] = base64_dec_map[*(cptr++)];
		while (*cptr == '\n' || *cptr == '\r' || *cptr == '\0') cptr++, i++, ws++;

		assert(enc[0] != 64 && enc[1] != 64);

		*decoded++ = enc[0] << 2 | enc[1] >> 4; 
		*decoded++ = (enc[1] & 0xF) << 4 | enc[2] >> 2; 
		*decoded++ = (enc[2] & 0x3) << 6 | enc[3]; 

		octects += 3;
		if (enc[2] == 64) octects--;
		if (enc[3] == 64) 
		{
			octects--;
			break;
		}
	}

	//cout << "Whitespaces : " << ws << " " << i << endl;
	return octects;
}

int64_t RSAX509 :: ReadPEMBlockFromFile(const string &filename, const string &marker, string &encodedBlock)
{
	ifstream keyfile(filename.c_str());
	stringstream filestream;
	string contents, encodedSection;
	string beginMarker, endMarker;
	uint64_t markerLen;
	size_t begin, end;

	if (!keyfile.is_open())
		return -1;

	filestream << keyfile.rdbuf();

	contents = filestream.str();

	beginMarker = "-----BEGIN ";
	beginMarker += marker;
	beginMarker += "-----\n";
	markerLen = beginMarker.length();

	endMarker = "\n-----END ";
	endMarker += marker;
	endMarker += "-----";

	begin = contents.find(beginMarker) + markerLen;
	end = contents.find(endMarker);

	if (begin == string::npos || end == string::npos)
		return -1;

	encodedBlock = contents.substr(begin, end-begin);

	return 0;
}


int64_t RSAX509 :: WritePEMBlockToFile(const string &filename, const string &marker, const string &encodedBlock)
{
	ofstream keyfile(filename.c_str());
	stringstream filestream;
	string contents, encodedSection;
	string beginMarker, endMarker;
	uint64_t markerLen;
	size_t begin, end;

	if (!keyfile.is_open()) {
		cout << "Failed to open file for writing." << endl;
		return -1;
	}

	keyfile << "-----BEGIN ";
	keyfile << marker;
	keyfile << "-----" << endl;

	keyfile << encodedBlock << endl;

	keyfile << "-----END ";
	keyfile << marker;
	keyfile << "-----" << endl;

	keyfile.close();
}


// Returns number of octects for tag 
// Need to add more parser error checking later
size_t RSAX509 :: DERParser :: ParseTag(uint8_t *ptr, uint64_t len, uint64_t *tag)
{
	uint8_t byte;
	uint64_t tagVal = 0; // assumes length fits in an uint64_t; a reasonable assumption
	uint64_t tagLen = 0;
	uint8_t *t = ptr;

	if (len <= 0)
	{
		*tag = 0;
	}
	else 
	{
		byte = *(t++) & 0x1F;

		if (byte < 0x1F)
		{
			*tag = byte;
		}
		else
		{
			// long form
			do
			{
				byte = (*t++);
				// 7 bits in each octect; big endian
				tagVal = tagVal * 128 + byte & 0x7F;
			
			} while (byte & 0x80);
	
			*tag = tagVal;	
		}
	}

	return (t - ptr);
}

// Returns number of octects for length
// Need to add more parser error checking later
size_t RSAX509 :: DERParser :: ParseLength(uint8_t *ptr, uint64_t len, uint64_t *length)
{
	uint8_t byte;
	uint64_t lenVal = 0;
	uint64_t lenLen = 0, i;
	uint8_t *t = ptr;

	if (len <= 0)
	{
		*length = 0;
	}
	else 
	{
		byte = *(t++);
		if (byte < 0x80)
		{
			*length = byte;
		}
		else
		{
			lenLen = byte & 0x7F;

			for(i = 0; i < lenLen; i++)
			{
				byte = *(t++);
				lenVal = lenVal * 256 + byte;
			}

			*length = lenVal;
		}
	}

	return (t - ptr);
}

size_t RSAX509 :: DERParser :: ParseInt(uint8_t *ptr, uint64_t len, BIGINTEGER *val)
{
	size_t l;
	uint64_t tag, length;
	uint8_t *p = ptr;
	l = ParseTag(p, len, &tag);
	if (l == 0 || (tag != 0x02))  // Integer tag
		return 0;
	p += l;
	len -= l;
	l = ParseLength(p, len, &length);
	if (l == 0 || (l + length) > len)
		return 0;
	p += l;
	len -= l;

	//hexdump(p, length, 1);
	*val = RSALib::MsgIntRep(p, length);
	p += length;
	len -= length;

	return p - ptr;
}

size_t RSAX509 :: DERParser :: ParseBitStringInt(uint8_t *ptr, uint64_t len, BIGINTEGER *val)
{
	size_t l;
	uint64_t tag, length;
	uint8_t *p = ptr;
	l = ParseTag(p, len, &tag);
	if (l == 0 || (tag != 0x03))  // Bit string tag
		return 0;
	p += l;
	len -= l;
	l = ParseLength(p, len, &length);
	if (l == 0 || (l + length) > len)
		return 0;
	p += l;
	len -= l;

	//hexdump(p, length, 1);
	*val = RSALib::MsgIntRep(p, length);
	p += length;
	len -= length;

	return p - ptr;
}

size_t RSAX509 :: DERParser :: ParseBitString(uint8_t *ptr, uint64_t len, uint8_t **bits, uint64_t *size)
{
	size_t l;
	uint64_t tag, length;
	uint8_t *p = ptr;
	l = ParseTag(p, len, &tag);
	if (l == 0 || (tag != 0x03))  // Bit string tag
		return 0;
	p += l;
	len -= l;
	l = ParseLength(p, len, &length);
	if (l == 0 || (l + length) > len)
		return 0;
	p += l;
	len -= l;

	//hexdump(p, length, 1);
	*bits = p;
	*size = length;

	return p - ptr;
}


size_t RSAX509 :: DERParser :: ParseSequence(uint8_t *ptr, uint64_t len, uint64_t *seqLen)
{
	size_t l;
	uint64_t tag, length;
	uint8_t *p = ptr;
	l = ParseTag(p, len, &tag);
	if (l == 0 || (tag != 0x10))  // Sequence tag - not that the higher order bits are already cleared
		return 0;
	p += l;
	len -= l;
	l = ParseLength(p, len, &length);
	if (l == 0 || (l + length) > len)
		return 0;
	p += l;

	*seqLen = length;

	return p - ptr;
}

size_t RSAX509 :: DERParser :: ParseGeneral(uint8_t *ptr, uint64_t len, uint64_t *tag, uint64_t *attrLen)
{
	size_t l;
	uint64_t length = 0;
	uint8_t *p = ptr;
	l = ParseTag(p, len, tag);
	if (l == 0)
		return 0;
	p += l;
	len -= l;

	if (tag != 0) //  NULL Tag doesn't have a length
	{
		l = ParseLength(p, len, &length);
		if (l == 0 || (l + length) > len)
			return 0;
		p += l;
	}

	*attrLen = length;

	return p - ptr;
}

int RSAX509 :: DERParser :: Parse(uint8_t *ptr, uint64_t len)
{
	size_t fieldLen;
	uint64_t tag, length;
	int rc;

	while (len)
	{
		fieldLen = ParseTag(ptr, len, &tag);
		if(tag == 0) //end of content padding; ignore
		{
			ptr++;
			len--;
			continue;
		}
		else
		{
			if (fieldLen >= len)
				return -1;
			ptr += fieldLen;
			len -= fieldLen;
		}

		fieldLen = ParseLength(ptr, len, &length);
		if((fieldLen + length) > len)
			return -1;

		ptr += fieldLen;
		len -= fieldLen;

		//ptr now points to data
		// handleData(ptr, length);

		if(tag == 0x10) // a sequence; get into that; recursively call Parse...
		{
			rc = Parse(ptr, len);
			if (rc == -1)
				return -1;
		}

		ptr += length;
		len -= length;
	}

	return 0;
}

size_t RSAX509 :: DERParser :: EncodeTag(uint64_t tag, uint8_t highbits, uint8_t *buffer)
{
	uint8_t str[16];
	uint64_t index = 0, index1; 
	if(tag < 0x1F)
	{
		*buffer = (uint8_t)tag | highbits;
		return 1;
	} 
	else
	{
		*buffer++ = 0x1F | highbits;
		while(tag)
		{
			str[15-index++] = tag & 0x7F;	
			tag = tag >> 7;
		}

		index1 = index;
		while(index--)
		{
			if(!index) 
				*buffer++ = str[15-index];
			else
				*buffer++ = 0x80 | str[15-index];
		}

		return index1 + 1;
	}
	
	return 0;
}


size_t RSAX509 :: DERParser :: EncodeLength(uint64_t length, uint8_t *buffer)
{
	uint8_t str[16] = {0};
	uint64_t index = 0, index1; 
	uint8_t *ptr;
	if(length < 0x80)
	{
		*buffer = (uint8_t)length;
		return 1;
	} 
	else
	{
		ptr = buffer;
		buffer++;
		while(length)
		{
			str[15-index++] = length & 0xFF ;	
			length = length >> 8;
		}

		index1 = index;
		while(index--)
		{
			*buffer++ = str[15-index];
		}

		*ptr = 0x80 | index1;

		return index1 + 1;
	}

	return 0;
}

size_t RSAX509 :: DERParser :: EncodeBigInt(BIGINTEGER *val, uint64_t tag, uint8_t *buffer)
{
	size_t attrlen;
	uint64_t len = RSALib::MaxChunkSize(*val);
	uint8_t *p = buffer;
	uint8_t len_ext = ((*val) >> (len * 8 - 4)) >= 0x8 ? 1 : 0;

	// encode the tag
	attrlen = EncodeTag(tag, 0x00, p);
	p += attrlen;
	
	attrlen = EncodeLength(len + len_ext, p);
	p += attrlen;

	// Resultant length could be greater by 1, if 0x00 was prepended
	len = RSALib::MsgStrRep(*val, p, len, true);
	p += len;

	return p - buffer;
}

void RSAX509 :: DERParser :: EncodePrivateKey (BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pubK, BIGINTEGER *pvtK, uint8_t *buffer, uint64_t *len, uint64_t *off)
{
//RSAPrivateKey ::= SEQUENCE {
//  version Version,
//  modulus INTEGER,
//  publicExponent INTEGER,
//  privateExponent INTEGER,
//  prime1 INTEGER,
//  prime2 INTEGER,
//  exponent1 INTEGER,
//  exponent2 INTEGER,
//  coefficient INTEGER
//}

	size_t attrlen;
	BIGINTEGER val;
	uint8_t localBuffer[16];
	// Reserve 4 bytes for tag and len;
	uint8_t *t = buffer + 16, *ptr = localBuffer;
	
	// Encode version
	*t++ = 0x02;
	*t++ = 0x01;
	*t++ = 0x00;

	// Modulus
	val = (*p) * (*q);	
	attrlen = EncodeBigInt(&val, 0x02, t);
	t += attrlen;

	//Public key
	attrlen = EncodeBigInt(pubK, 0x02, t);
	t += attrlen;

	//Private key
	attrlen = EncodeBigInt(pvtK, 0x02, t);
	t += attrlen;

	//p
	attrlen = EncodeBigInt(p, 0x02, t);
	t += attrlen;

	//q
	attrlen = EncodeBigInt(q, 0x02, t);
	t += attrlen;

	// Exponent1
	// Just fill p for now - correct later
	attrlen = EncodeBigInt(p, 0x02, t);
	t += attrlen;

	// Exponent2
	// Just fill p for now - correct later
	attrlen = EncodeBigInt(p, 0x02, t);
	t += attrlen;

	//Coefficient
	// Just fill p for now - correct later
	attrlen = EncodeBigInt(p, 0x02, t);
	t += attrlen;
	
	attrlen = EncodeTag(0x10, 0x20, ptr);	
	ptr += attrlen;
	attrlen = EncodeLength(t - buffer - 16, ptr);
	ptr += attrlen;

	attrlen = ptr - localBuffer;

	*off = 16 - attrlen;
	*len = t - buffer - *off;

	memcpy(buffer + *off, localBuffer, attrlen);
	//hexdump (buffer + *off, *len, 1);
}

void RSAX509 :: DERParser :: EncodePublicKey (BIGINTEGER *modulus, BIGINTEGER *pubK, uint8_t *buffer, uint64_t *len, uint64_t *off)
{
//RSAPublicKey ::= SEQUENCE {
//   modulus          INTEGER, -- (Usually large) n = p*q
//   publicExponent   INTEGER  -- (Usually small) e 
// }

	size_t attrlen;
	BIGINTEGER val;
	uint8_t localBuffer[32];
	// Reserve 4 bytes for tag and len;
	uint8_t *t = buffer + 32, *ptr = localBuffer;
	uint8_t rsaPubSeq[] = {0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00}; 

	// Modulus	
	attrlen = EncodeBigInt(modulus, 0x02, t);
	t += attrlen;

	//Public key
	attrlen = EncodeBigInt(pubK, 0x02, t);
	t += attrlen;

	attrlen = EncodeTag(0x10, 0x20, ptr);	
	ptr += attrlen;
	attrlen = EncodeLength(t - buffer - 32, ptr);
	ptr += attrlen;

	attrlen = ptr - localBuffer;

	*off = 32 - attrlen;

	memcpy(buffer + *off, localBuffer, attrlen);

	// encode the encapsulating object
	ptr = localBuffer;
	attrlen = EncodeTag(0x03, 0x00, ptr);
	ptr += attrlen;
	attrlen = EncodeLength(t - buffer - *off + 1, ptr); // 1 is for null tag
	ptr += attrlen;
	*ptr++ = 0x00;  // A null tag.. don't know why!!
	
	attrlen = ptr - localBuffer;
	*off -= attrlen;
	memcpy(buffer + *off, localBuffer, attrlen);

	// Copy in the pubkey seq
	*off -= 0x0f; //length of seq
	memcpy(buffer + *off, rsaPubSeq, 0x0f);

	// Encode the outer sequence
	ptr = localBuffer;
	attrlen = EncodeTag(0x10, 0x20, ptr);
	ptr += attrlen;
	attrlen = EncodeLength(t - buffer - *off, ptr);
	ptr += attrlen;

	attrlen = ptr - localBuffer;
	*off -= attrlen;
	memcpy(buffer + *off, localBuffer, attrlen);

	*len = t - buffer - *off;

	//hexdump (buffer + *off, *len, 1);
}

int RSAX509 :: DERParser :: WriteKeyFile(string filename, BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pubK, BIGINTEGER *pvtK)
{
	uint8_t buffer[4096];
	uint64_t len, off;
	string encoded;

	EncodePrivateKey (p, q, pubK, pvtK, buffer, &len, &off);
	RSAX509::Base64Encode(buffer+off, len, encoded);
	RSAX509::WritePEMBlockToFile(filename, MARKER_PVT_KEY, encoded);	

	return 0;
}

int RSAX509 :: DERParser :: WritePublicKeyFile(string filename, BIGINTEGER *modulus, BIGINTEGER *pubK)
{
	uint8_t buffer[4096];
	uint64_t len, off;
	string encoded;

	EncodePublicKey (modulus, pubK, buffer, &len, &off);
	RSAX509::Base64Encode(buffer+off, len, encoded);
	RSAX509::WritePEMBlockToFile(filename, MARKER_PUB_KEY, encoded);	

	return 0;
}

int RSAX509 :: DERParser :: ReadKeyFile(string filename, BIGINTEGER *pr1, BIGINTEGER *pr2, BIGINTEGER *pubK, BIGINTEGER *pvtK)
{
	uint8_t buffer[4096];
	uint64_t len;
	size_t off;	
	int errored = 0;
	string encoded;
	BIGINTEGER temp;
	uint64_t seqLen;
	
	if (RSAX509::ReadPEMBlockFromFile(filename.c_str(), MARKER_PVT_KEY, encoded) == -1)
		return -2;
	
	len = RSAX509::Base64Decode(encoded, buffer);

	//hexdump(buffer, len, 1);

	do
	{
		uint8_t *p = buffer;
		off = ParseSequence(p, len, &seqLen);

		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Read version
		off = ParseInt(p, len, &temp);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, &temp);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, pubK);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, pvtK);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, pr1);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, pr2);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Exponent 1
		off = ParseInt(p, len, &temp);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Exponent 2
		off = ParseInt(p, len, &temp);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Exponent 3
		off = ParseInt(p, len, &temp);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		if (len != 0)
		{
			cout << "There is residue" << endl;
			errored = 1;
			break;
		}

	} while(0);

	if (errored)
	{
		cout << "Failed to read key from file" << endl;
		return -1;
	}

#if 0
	cout << "Pvt Key = " << *pvtK << endl;
	cout << "Pub Key = " << *pubK << endl;
	cout << "P = " << *pr1 << endl;
	cout << "Q = " << *pr2 << endl;
#endif

	return 0;
}


int RSAX509 :: DERParser :: ParsePublicKey(uint8_t *ptr, uint64_t len, BIGINTEGER *modulus, BIGINTEGER *pubK)
{
	uint64_t val, seqLen;
	size_t off;	
	int errored = 0;

	do
	{
		uint8_t *p = ptr; // gets us all the way into the rsa key sequence
		off = ParseSequence(p, len, &seqLen);
		if (off == 0)
		{
			errored = 1;
			break;
		}
		p += off;
		len -= off;

		off = ParseSequence(p, len, &seqLen);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += (off + seqLen);
		len -= (off + seqLen);

		// Now the RSA encryption object
		off = ParseTag(p, len, &val);
		if (off == 0 || val != 0x03) // object tag
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;
		
		off = ParseLength(p, len, &val);
		if (off == 0) 
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Read out any NULL tags
		while(*p == 0x00) {
			len --;
			p++;
		}
		
		off = ParseSequence(p, len, &seqLen);

		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		// Read version
		off = ParseInt(p, len, modulus);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;

		off = ParseInt(p, len, pubK);
		if (off == 0)
		{
			errored = 1;
			break;
		}

		p += off;
		len -= off;
		if (len != 0)
		{
			cout << "There is residue " << len << " bytes left." << endl;
			errored = 1;
			break;
		}

	} while(0);

	if (errored)
	{
		cout << "Failed to read key from file" << endl;
		return -1;
	}

	return 0;

}

int RSAX509 :: DERParser :: ReadPublicKeyFile(string filename, BIGINTEGER *modulus, BIGINTEGER *pubK)
{
	uint8_t buffer[4096];
	string encoded;
	uint64_t len;

	if (RSAX509::ReadPEMBlockFromFile(filename.c_str(), MARKER_PUB_KEY, encoded) == -1)
		return -2;
	
	len = RSAX509::Base64Decode(encoded, buffer);

	//hexdump(buffer, len, 1);

	if (ParsePublicKey(buffer, len, modulus, pubK) != 0)
		return -1;
#if 0
	cout << "Modulus: " << *modulus << endl;
	cout << "Exponent: " << *pubK << endl;
#endif

	return 0;
}


int RSAX509 :: DERParser :: ParseSubject(uint8_t *p, uint64_t len1, string &subject)
{
	size_t size;
	uint64_t t, l;
	uint64_t t1, t2;
	char subj[128];
	uint8_t CNCode[] = {0x06, 0x03, 0x55, 0x04, 0x03, 0x0c};
	int found = 0;


	size = ParseGeneral(p, len1 , &t, &l);
	if (size == 0)
		return -1;
	//hexdump (ptr, l + size, 1);

	p += size;
	len1 -= size;

	while (len1 > 2) // The min for type and len
	{
		// Outer set
		t1 = ParseGeneral(p, len1, &t, &t2);
		if (t1 == 0)
			return -1;

		p += t1;
		len1 -= t1;

		t1 = ParseGeneral(p, len1, &t, &t2);
		if (t1 == 0)
			return -1;

		p += t1;
		len1 -= t1;

		//hexdump(p, t2, 1);

		if (t2 > 7 && memcmp(p, CNCode, 6) == 0)
		{
			t1 = ParseLength(p + 6, len1 - 6, &t);
			memcpy(subj, p + 6 + t1, t);
			subj[t] = '\0';
			subject = string(subj);	
			found = 1;
			break;
		}

		p += t2;
		len1 -= t2;
	}


#if 0
	if (!found)
		return -1;
#endif

	return 0;
}

int RSAX509 :: DERParser :: ParseCertValid(uint8_t *cert, uint64_t len, bool *timeValid, string &subject, string &issuer, BIGINTEGER *modulus, BIGINTEGER *pubK)
{
	uint8_t *ptr = cert;
	size_t size;
	uint64_t t, l;
	struct tm tm1, tm2;
	time_t time1, time2, curtime;
	uint8_t *issuerData;
	uint64_t issuerLen;
	int rc;

	// Parse outer sequence;
	size = ParseSequence(ptr, len, &l);
	if (size == 0)
		return -1;

	ptr += size;
	len -= size;

	// Skip version
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0)
		return -1;


	// Still haven't unearthed the version mystery!
#if 0
	ptr += (size + l);
	len -= (size + l);
#else
	if (t != 0x02)
	{
		// This is version encoded..
		ptr += 5;
		len -= 5;
	}
#endif

	//Skip serial number	
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0)
		return -1;

	ptr += (size + l);
	len -= (size + l);

	// Skip signature
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0)
		return -1;
	ptr += (size + l);
	len -= (size + l);

	// Skip issuer 
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0)
		return -1;
	issuerData = ptr;
	issuerLen = l;
	rc = ParseSubject (ptr, len, issuer);
#if 0
	if (rc != 0)
		return -1;
#endif

	ptr += (size + l);
	len -= (size + l);

	// Skip validity 
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0 || l != 30)
		return -1;

	tm1.tm_year = 100 + (*(ptr + 4) - '0')*10 + (*(ptr + 5) - '0');
	if (tm1.tm_year >= 150) 
		tm1.tm_year -= 100;

	tm1.tm_mon = (*(ptr + 6) - '0')*10 + (*(ptr + 7) - '0') - 1;
	tm1.tm_mday = (*(ptr + 8) - '0')*10 + (*(ptr + 9) - '0') - 1;
	tm1.tm_hour = (*(ptr + 10) - '0')*10 + (*(ptr + 11) - '0') - 1;
	tm1.tm_min = (*(ptr + 12) - '0')*10 + (*(ptr + 13) - '0') - 1;
	tm1.tm_sec = (*(ptr + 14) - '0')*10 + (*(ptr + 15) - '0') - 1;

	tm2.tm_year = 100 + (*(ptr + 15 + 4) - '0')*10 + (*(ptr + 15 + 5) - '0');
	if (tm2.tm_year >= 150) 
		tm2.tm_year -= 100;

	tm2.tm_mon = (*(ptr + 15 + 6) - '0')*10 + (*(ptr + 15 + 7) - '0') - 1;
	tm2.tm_mday = (*(ptr + 15 + 8) - '0')*10 + (*(ptr + 15 + 9) - '0');
	tm2.tm_hour = (*(ptr + 15 + 10) - '0')*10 + (*(ptr + 15 + 11) - '0');
	tm2.tm_min = (*(ptr + 15 + 12) - '0')*10 + (*(ptr + 15 + 13) - '0');
	tm2.tm_sec = (*(ptr + 15 + 14) - '0')*10 + (*(ptr + 15 + 15) - '0');

	time1 = mktime(&tm1);
	time2 = mktime(&tm2);
	curtime = time(NULL);

	if (curtime < time1 || curtime > time2) {
		*timeValid = false;
		return -1;	
	}
		

	ptr += (size + l);
	len -= (size + l);

	// Skip subject 
	size = ParseGeneral(ptr, len , &t, &l);
	if (size == 0)
		return -1;
	//hexdump (ptr, l + size, 1);

	if (issuerLen != l || memcmp(issuerData, ptr, l) != 0)
		return -2; // Not self signed

	rc = ParseSubject (ptr, len, subject);
#if 0
	if (rc != 0)
		return -1;
#endif

	ptr += (size + l);
	len -= (size + l);

	// We are at public key info
	size = ParseSequence(ptr, len , &l);
	if (size == 0)
		return -1;

	//hexdump(ptr, l + size, 1);

	if (ParsePublicKey(ptr, l + size, modulus, pubK) != 0)
		return -1;

	return 0;
	
}

int RSAX509 :: DERParser :: CertGetPubKey(string filename, BIGINTEGER *modulus, BIGINTEGER *pubKey)
{
	uint8_t buffer[4096];
	size_t len, msgLen, size;
	string encoded;
	uint8_t *ptr = buffer;
	uint64_t seqLen;
	string subject, issuer;
	bool timeValid = true;
	int rc;

	if (RSAX509::ReadPEMBlockFromFile(filename.c_str(), MARKER_CERTIFICATE, encoded) == -1)
		return -2;
	
	len = RSAX509::Base64Decode(encoded, buffer);

	size = ParseSequence(ptr, len, &seqLen);
	if (size == 0)
		return -1;
	ptr += size;
	len -= size;

	// Certificate comes here!!
	// Find length of certificate.
	size = ParseSequence(ptr, len, &seqLen);
	if (size == 0)
		return -1;

	rc = ParseCertValid(ptr, size + seqLen, &timeValid, subject, issuer, modulus, pubKey);

	return rc;
}


int RSAX509 :: DERParser :: CertVerifySign(string filename, string &subject, string &issuer)
{
	uint8_t buffer[4096];
	size_t len, msgLen;
	BIGINTEGER modulus, pubK;
	string encoded;
	uint8_t *ptr = buffer;
	uint8_t *decrypted = ptr + 2048;
	uint8_t *sign;
	size_t size;
	uint64_t val, seqLen;
	RSALib rsa;
	uint8_t sha1HashHeader[] = { 0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14};
	uint8_t sha1Hash[20];
	bool timeValid = true;
	int rc;

	if (RSAX509::ReadPEMBlockFromFile(filename.c_str(), MARKER_CERTIFICATE, encoded) == -1)
		return -1;
	
	len = RSAX509::Base64Decode(encoded, buffer);

	//hexdump(ptr, len, 1);

	size = ParseSequence(ptr, len, &seqLen);
	if (size == 0)
		return -1;
	ptr += size;
	len -= size;

	// Certificate comes here!!
	// Find length of certificate.
	size = ParseSequence(ptr, len, &seqLen);
	if (size == 0)
		return -1;

	//hexdump(ptr, size +seqLen, 1);	
	// Calculate hash of cert
	SHA1Hash(ptr, size + seqLen, sha1Hash);
	//hexdump (sha1Hash, 20, 1);

	rc = ParseCertValid(ptr, size + seqLen, &timeValid, subject, issuer, &modulus, &pubK);

	if (rc == -2)
		return -3;  // Not self-signed

	if (!timeValid)
		return -2;

	if (rc != 0 )
		return -1;

#if 0
	cout << "Modulus : " << hex << modulus << endl;
	cout << "Public Key : " << hex << pubK << endl;
#endif

	// Skip over the certificate
	ptr += (size + seqLen);
	len -= (size + seqLen);

	// We are at signature now!
	//hexdump(ptr, len, 1);
	size = ParseSequence(ptr, len, &seqLen);
	ptr += size;
	len -= size;

	// Skip over algo identifier
	ptr += seqLen;
	len -= seqLen;
	
	//hexdump(ptr, len, 1);

	size = ParseBitString(ptr, len, &sign, &val);
	//size = ParseSequence(ptr, len, &seqLen);
	ptr += size;
	len -= size;

	uint64_t keySize = RSALib::MaxChunkSize(modulus);
	uint64_t offset = val % keySize;
	assert (offset <= 1);
	ptr += offset;

	//hexdump(ptr, keySize, 1);
	if (rsa.Decrypt(ptr, val - offset, pubK, modulus, decrypted, &msgLen, 1) != 0)
		return -1;
	//hexdump(decrypted,  msgLen, 1);
	//hexdump(decrypted + sizeof(sha1HashHeader),  msgLen - sizeof(sha1HashHeader), 1);

	
	if (memcmp(decrypted, sha1HashHeader, sizeof(sha1HashHeader)) != 0 ||
	    memcmp(sha1Hash, decrypted + sizeof(sha1HashHeader), 20) != 0)
		return -1;
	
	return 0;
}

int RSAX509 :: DERParser :: WriteCert(string filename, string keyfile, BIGINTEGER seqNo)
{
	uint8_t buffer[4096];
	uint8_t buffer1[4096];
	uint64_t len = 0, off, length;
	size_t size;
	time_t curTime;
	struct tm *ptm;
	int val, iter = 0, start = 0, addZero = 0;
	BIGINTEGER pr1, pr2, modulus, pvtK, pubK;
	string encoded;
	uint8_t version[] = {0xa0, 0x03, 0x02, 0x01, 0x02};
	uint8_t signature[] = {0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x05, 0x05, 0x00};
	uint8_t sha1HashHeader[35] = { 0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14};
	// Encodes the identity: '/C=US/ST=NY/L=StonyBrook/CN=www.bineshandrews.com/email=bineshandrews@gmail.com'
	uint8_t subject[] = { 0x30, 0x70, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x55, 0x53, 0x31, 
	                      0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x08, 0x0c, 0x02, 0x4e, 0x59, 0x31, 0x13, 0x30, 0x11, 
	                      0x06, 0x03, 0x55, 0x04, 0x07, 0x0c, 0x0a, 0x53, 0x74, 0x6f, 0x6e, 0x79, 0x42, 0x72, 0x6f, 0x6f, 
	                      0x6b, 0x31, 0x17, 0x30, 0x15, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0e, 0x42, 0x69, 0x6e, 0x65,
	                      0x73, 0x68, 0x20, 0x41, 0x6e, 0x64, 0x72, 0x65, 0x77, 0x73, 0x31, 0x26, 0x30, 0x24, 0x06, 0x09,
	                      0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x09, 0x01, 0x16, 0x17, 0x62, 0x69, 0x6e, 0x65, 0x73,
	                      0x68, 0x61, 0x6e, 0x64, 0x72, 0x65, 0x77, 0x73, 0x40, 0x67, 0x6d, 0x61, 0x69, 0x6c, 0x2e, 0x63,
	                      0x6f, 0x6d};

	uint8_t *ptr = buffer;
	uint8_t *ptr1 = buffer1;
	RSALib rsa;

	if (ReadKeyFile(keyfile, &pr1, &pr2, &pubK, &pvtK) != 0)
		return -1;

	modulus = pr1 * pr2;

	ptr += 16; // Leave space to encode the outer sequence header and cert seq header

#if 0
	memcpy(ptr, version, sizeof(version));
	ptr += sizeof(version);
	len += sizeof(version);
#endif

	size = EncodeBigInt(&seqNo, 0x02, ptr);
	ptr += size;
	len += size;

	memcpy(ptr, signature, sizeof(signature));
	ptr += sizeof(signature);
	len += sizeof(signature);

	memcpy(ptr, subject, sizeof(subject));
	ptr += sizeof(subject);
	len += sizeof(subject);

	//Encode validity.
	*ptr++ = 0x30;
	*ptr++ = 0x1e;	
	*ptr++ = 0x17; //Date
	*ptr++ = 0xd;
	//Encode current time here
	curTime = time(NULL);
	ptm = gmtime(&curTime);

	do 
	{
		val = ptm->tm_year % 100;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		val = ptm->tm_mon;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		val = ptm->tm_mday;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		val = ptm->tm_hour;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		val = ptm->tm_min;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		val = ptm->tm_sec;
		*ptr++ = (val / 10) + '0';
		*ptr++ = (val % 10) + '0';
		*ptr++ = 'Z';

		if (iter == 0)
		{
			//Expires in 365 days
			curTime += (86400ULL * 365);
			ptm = gmtime(&curTime);
		
			*ptr++ = 0x17; //Date
			*ptr++ = 0xd;
		}
	} while(iter++ < 1);
		
	len += 32;

	memcpy(ptr, subject, sizeof(subject));
	ptr += sizeof(subject);
	len += sizeof(subject);

	EncodePublicKey (&modulus, &pubK, ptr1, &length, &off);
	memcpy(ptr, ptr1 + off, length); 
	ptr += length;
	len += length;

	ptr1 = buffer1;
	*ptr1++ = 0x30;
	size = EncodeLength(len, ptr1);
	size++;
	ptr1 = buffer;
	ptr1 += (16 - size);
	start = 16 - size;
	memcpy(ptr1, buffer1, size);

	len += size;

	//hexdump(ptr1, len, 1);
	SHA1Hash(ptr1, len, &sha1HashHeader[15]);
	//hexdump(&sha1HashHeader[15], 20, 1);

	memcpy(ptr, signature, sizeof(signature));
	ptr += sizeof(signature);
	len += sizeof(signature);

	rsa.Encrypt(sha1HashHeader, sizeof(sha1HashHeader), pvtK, modulus, buffer1, &length, 1);
	//hexdump(buffer1, length, 1);
	if (buffer1[0] >= 0x80) 
		addZero = 1;
	*ptr++ = 0x03;
	len++;
	size = EncodeLength(length + addZero, ptr);
	ptr += size;
	len += size;
	if (addZero)
		*ptr++ = 0x00;

	memcpy(ptr, buffer1, length + addZero);
	ptr += (length + addZero);
	len += (length + addZero);

	ptr1 = buffer1;
	*ptr1++ = 0x30;
	size = EncodeLength(len, ptr1);
	size++;

	ptr1 = buffer;
	start -= size;
	ptr1 += start;
	memcpy(ptr1, buffer1, size);

	len += size;

	//hexdump (ptr1, len, 1);
	Base64Encode(ptr1, len, encoded);
	RSAX509::WritePEMBlockToFile(filename, MARKER_CERTIFICATE, encoded);	

	return 0;
}


int RSAX509 :: DERParser :: SignFileHash(string filename, string keyfile, string signature, string outfile)
{
	int rc;
	RandNumIterator rit(64);
	BIGINTEGER pr1, pr2, pubK, pvtK, modulus, seqNum = *++rit;
	uint8_t sha1Buf[35] = { 0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14};
	RSALib rsa;
	uint8_t buffer[4096];
	uint64_t length;
	uint8_t *ptr = sha1Buf;

	rc = ReadKeyFile(keyfile, &pr1, &pr2, &pubK, &pvtK);
	if (rc != 0)
		return rc;

	modulus = pr1 * pr2;

	// Find SHA1 of file
	if (Sha1File(filename, ptr + 15) != 0)
		return -1;

	//hexdump(sha1Buf, 20, 1);
	rsa.Encrypt(sha1Buf, sizeof(sha1Buf), pvtK, modulus, buffer, &length, 1);

	ofstream outF(signature.c_str());
	if (!outF.is_open()) {
		cout << "Could not open output file" << endl;
		return -1;
	}
	outF.write((char *)buffer, length);
	outF.close();

	WriteCert(outfile, keyfile, seqNum);
	
	return 0;
}

int RSAX509 :: DERParser :: VerifyFileHash(string filename, string hashfilename, BIGINTEGER *modulus, BIGINTEGER *pubK)
{
	// 20 bytes of sha1 buffer + 15 bytes algo identifier
	uint8_t sha1Buf[35] = { 0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14};
	uint8_t decMsg[4096];
	RSALib rsa;
	uint8_t outBuf[4096];
	char buffer[4096];
	uint64_t decLen;
	int rc;
	uint8_t *ptr = decMsg;
	uint8_t *ptr1 = sha1Buf;
	size_t totalLen = 0;
	size_t readsize, blocksize = RSALib::MaxChunkSize(*modulus);

	rc = 0;
	ifstream inFile(hashfilename.c_str(), ios::binary);
	while(inFile)
	{
		inFile.read((char *)buffer, blocksize);
		readsize = inFile.gcount();

		if (readsize == 0)
			break;

		if (readsize != blocksize)
		{
			rc = -1;
			break;
		}
		rc = rsa.Decrypt((uint8_t *)buffer, readsize, *pubK, *modulus, (uint8_t *)outBuf, &decLen, 1);
		if (rc != 0)
			break;

		memcpy(ptr, outBuf, decLen);
		ptr += decLen;
		totalLen += decLen;
	}

	inFile.close();

	if (rc != 0)
		return -1;

	if (totalLen != 35)
		return -1;

	//hexdump(decMsg, 35, 1);
	// Find SHA1 of file
	if (Sha1File(filename, ptr1 + 15) != 0)
		return -1;

	//hexdump(sha1Buf, 35, 1);

	if (memcmp(sha1Buf, decMsg, 35) != 0)
		return -1;

	return 0;
}

int RSAX509 :: DERParser :: VerifyFileHashCert(string filename, string hashfilename, string certfile)
{
	string subject, issuer;
	BIGINTEGER modulus, pubK;
	int ret;

	ret = CertVerifySign(certfile, subject, issuer); 
	if (ret != 0) 
	{
		if (ret == -2)
			cout << "Certificate validity expired!" << endl;
		else if (ret == -3)
			cout << "Certificate is not self signed - Unable to verify!" << endl;
		else 
			cout << "Certificate failed sign verification! [subject: " << subject << ", issuer: " << issuer <<" ]" << endl;
		return -1;
	}

	cout << "Certificate sign verified OK! [subject: " << subject << ", issuer: " << issuer <<" ]" << endl;

	CertGetPubKey(certfile, &modulus, &pubK);
	
	return VerifyFileHash(filename, hashfilename, &modulus, &pubK);	
}


int RSAX509 :: DERParser :: VerifyFileHashKeyFile(string filename, string hashfilename, string pubKeyFile)
{
	BIGINTEGER modulus, pubK;
	if (ReadPublicKeyFile(pubKeyFile, &modulus, &pubK) != 0) 
	{
		cout << "Failed to retrieve public key info from file." << endl;
		return -1;
	}

	return VerifyFileHash(filename, hashfilename, &modulus, &pubK);	
}
