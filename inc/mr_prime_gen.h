/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Prime number generator. Implements Miller Rabin and use it to
generate primes. 

Last updated on: 8-OCT-2012
*******************************************************************************/

#ifndef __MR_PRIME_GEN_H__
#define __MR_PRIME_GEN_H__

#include "biginteger.h"
#include "random_number.h"

#define NUM_WITNESS 20 

using namespace std;


class MillerRabinPrimeGenerator
{
	public:

		class PrimeGenIterator : public iterator<input_iterator_tag, int>
		{
			private:
				// We might create far more iterator instances than the Generator
				// Makes sense to have its own state for each iterator.
				BIGINTEGER prime;
				RandNumIterator rIter;

				// Do the magic; Generate another prime number.
				bool IsPrime(BIGINTEGER candidate);
				void GeneratePrime();
		
			public:
				PrimeGenIterator(uint32_t bitsize);
				PrimeGenIterator(const PrimeGenIterator& pgtor);

				// The two operations we support on this iterator is:
				// ++ - To generate a new prime number
				// * - Retrieve the current number.
				// == - Check whether a given number is prime or not
				// Feel free to use ++*iter to use both goodies together.
				bool operator ==(BIGINTEGER testprime);
				PrimeGenIterator& operator++();
				BIGINTEGER& operator*();
			
		};
};

#endif
