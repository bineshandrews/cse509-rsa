/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: Implements helper routines 

Last updated on: 8-OCT-2012
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctype.h>
#include <openssl/sha.h>

#include "utils.h"

using namespace std;

void hexdump(uint8_t *data, uint64_t length, int mode)
{
	// Mode denotes whether the character form needs to be printed.

	uint64_t data_off = reinterpret_cast<uint64_t>(data) - 0;
	uint64_t begin_off = data_off % 16;
	uint64_t end_off = (data_off + length) % 16;
	uint64_t begin_addr = data_off - begin_off;
	uint64_t end_addr = data_off + length - (end_off ? end_off : 16);
	uint64_t i, j;
	uint8_t *t = data;
	uint8_t val[16];

	cout.flush();
	for (i = begin_addr; i <= end_addr; i += 16)
	{
		cout << setfill('0') << setw(16) << hex << i <<":   ";
		for (j = 0; j < 16; j++)
		{
			if((i == begin_addr && j < begin_off) ||
			   (i == end_addr && end_off && j >= end_off)) {
				val[j] = 0;
				cout << "   ";
			}
			else  {
				val[j] = *(t++);
				cout << setfill('0') << setw(2) << hex << (int)val[j] << " ";
			}
		}

		if (mode == 0)
		{
			cout << endl;
			continue;
		}

		cout << " |   ";
		for (j = 0; j < 16; j++) {
			if((i == begin_addr && j < begin_off) ||
			   (i == end_addr && end_off && j >= end_off)) {
				cout << "  ";
			}
			else
			{
				cout << ((val[j] > 32 && val[j] < 127)? (char) val[j] : '.') << " ";
			}
		}
		cout << endl;
	}
}

unsigned char *SHA1Hash(uint8_t *ptr, uint64_t len, uint8_t *digest)
{
	return SHA1(ptr, len, digest);
}

int Sha1File(string filename, uint8_t *out)
{
	SHA_CTX sc;
	char buffer[4096];
	uint64_t blocksize = 4096;
	size_t readsize;

	ifstream inFile(filename.c_str(), ios::binary);

	if (!inFile)	
	{
		//cout << "Failed to open source file." << endl;
		return -1;
	}

	SHA1_Init(&sc);
	while(inFile)
	{
		inFile.read(buffer, blocksize);
		readsize = inFile.gcount();
		if (readsize == 0)
			break;

		SHA1_Update(&sc, buffer, readsize);
	}

	SHA1_Final(out, &sc);
	inFile.close();
	return 0;
}
