USE_GMP_PRIMES=#-DUSE_GMP_PRIMES
COMPILE_TEST_CASES=-DCOMPILE_TEST_CASES
OPT=-O3
CFLAGS=$(OPT) $(USE_GMP_PRIMES) $(COMPILE_TEST_CASES)
LIBS=-lssl -lcrypto -lgmp -lgmpxx

all:
	g++ $(CFLAGS) -Iinc -c src/utils.cpp -o bin/objs/utils.o
	g++ $(CFLAGS) -Iinc -c src/random_number.cpp -o bin/objs/random_number.o
	g++ $(CFLAGS) -Iinc -c src/mr_prime_gen.cpp -o bin/objs/mr_prime_gen.o
	g++ $(CFLAGS) -Iinc -c src/rsa_lib.cpp -o bin/objs/rsa_lib.o
	g++ $(CFLAGS) -Iinc -c src/rsa_x509.cpp -o bin/objs/rsa_x509.o
	g++ $(CFLAGS) -Iinc -c src/rsa_cmdline.cpp -o bin/objs/rsa_cmdline.o
	g++ $(CFLAGS) -Iinc -c test/test_rsa.cpp -o bin/test/test_rsa.o
	g++ bin/objs/utils.o bin/objs/random_number.o bin/objs/mr_prime_gen.o bin/objs/rsa_lib.o bin/objs/rsa_x509.o bin/test/test_rsa.o -o bin/out/test_rsa.out $(LIBS) 
	g++ bin/objs/utils.o bin/objs/random_number.o bin/objs/mr_prime_gen.o bin/objs/rsa_lib.o bin/objs/rsa_x509.o bin/objs/rsa_cmdline.o -o bin/out/rsaeng.out $(LIBS) 

clean:
	rm -rf bin/objs/*.o
	rm -rf bin/test/*.o
	rm -rf bin/out/*.out 

install_mpz:
	./configure --enable-cxx
	make -j5
	sudo make install
	export LD_LIBRARY_PATH=/usr/local/lib
