/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: This file includes definitions for two iterators (one using the 
big integer library and the other without), for generating random numbers. 
The big integer version can create random numbers of any bitsize with 
sufficiently flat distribution (using the mersenne twister algorithm.)

Last updated on: 8-OCT-2012
*******************************************************************************/

#ifndef __RANDOM_NUMBER_H__
#define __RANDOM_NUMBER_H__

#include <stdint.h>
#include "biginteger.h"

using namespace std;

class RandNumIterator : public iterator<input_iterator_tag, int>
{
	gmp_randclass mr_rstate;
	uint32_t mr_bitsize;
	BIGINTEGER randnum;

	public:
	RandNumIterator(uint32_t);
	RandNumIterator(const RandNumIterator&);

	RandNumIterator& operator++();
	BIGINTEGER operator*();
	BIGINTEGER inRange(BIGINTEGER rangemax);
};

class MiniRandNumIterator : public iterator<input_iterator_tag, int>
{
	uint32_t seed;
	uint32_t randnum;

	public:
	MiniRandNumIterator();
	MiniRandNumIterator(const MiniRandNumIterator&);

	MiniRandNumIterator& operator++();
	uint32_t operator*();
	uint32_t inRange(uint32_t rangemax);
};

#endif
