/*******************************************************************************
Author: Binesh Andrews <bineshandrews@gmail.com>

Description: RSA library. Definitions for generating RSA keys, and for
encryption and decryption using the keys 

Last updated on: 8-OCT-2012
*******************************************************************************/

#ifndef __RSA_LIB_H__
#define __RSA_LIB_H__

#include <stdint.h>
#include "biginteger.h"

enum STATUS {OK, ERROR=-1};

// 8 + 3
#define PKCS_PAD_OVERHEAD 11

class RSALib
{
	private:
		STATUS FindMultiplicativeInvMod(BIGINTEGER *phi, BIGINTEGER *e, BIGINTEGER *d);
		STATUS EncryptChunk(uint8_t *msg, uint64_t msgLen, BIGINTEGER key, BIGINTEGER modulus, uint8_t *encMsg, uint8_t type);
		STATUS DecryptChunk(uint8_t *encMsg, BIGINTEGER key, BIGINTEGER modulus, uint8_t *msg, uint64_t *msgLen, uint8_t type);

	// Temp.. make private after testing
	public:
		static BIGINTEGER MsgIntRep(uint8_t *msg, uint32_t len);
		static uint64_t MsgStrRep(BIGINTEGER intrep, uint8_t *str, uint64_t len, bool prepZero);
	public:
		// Find private key:
		// Find x or y from d * e = 1 mod phi 
		// Given phi = (p-1)*(q-1) and public key e, find private key d
		// Attacker doesn't know phi and hence cannot compute this.
		// Also it is a good idea to pass the pointer of the data
		// rather than passing the keys by value. This makes it easy to
		// clean up after we are done with our computation.
		STATUS CalculatePrivateKey(BIGINTEGER *phi, BIGINTEGER *pubKey, BIGINTEGER *pvtKey);
		void GenerateKeySet(uint64_t bitSize, BIGINTEGER *pubKey, BIGINTEGER *p, BIGINTEGER *q, BIGINTEGER *pvtKey);
		static size_t MaxChunkSize(BIGINTEGER modulus);
		static size_t MaxMsgChunkSize(BIGINTEGER modulus);

		STATUS Encrypt(uint8_t *msg, uint64_t msgLen, BIGINTEGER key, BIGINTEGER modulus, uint8_t *encMsg, uint64_t *encMsgLen, uint8_t type = 0);
		STATUS Decrypt(uint8_t *encMsg, uint64_t encMsgSize, BIGINTEGER key, BIGINTEGER modulus, uint8_t *msg, uint64_t *msgLen, uint8_t type = 0);
};

#endif
